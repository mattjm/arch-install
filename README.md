# arch-install

This is a basic script for my own purposes to bootstrap a basic Arch Linux install.

It's currently a WIP, but the goal is eventually to have a script that can install Arch and automatically configure my most frequently used software with my preferences. The script is not fully unattended as I only intend to use this on personal devices from time to time.
