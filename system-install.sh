#!/bin/bash

# start a server and fetch script from another device:
# python3 -m http.server
# curl http://hostname:8000/system-install.sh > install.sh && chmod +x ./install.sh

set -e

MAIN_DISK="${1}"
HOSTNAME="${2}"
USERNAME="${3}"

function becho() {
   echo "--> $(tput setaf 6)$1$(tput sgr0)"
}

function recho() {
     echo "!!> $(tput setaf 1)$1$(tput sgr0)"
}


if [ $# != 3 ]; then
  recho "Disk, hostname and username arguments must be supplied and are the only supported arguments."
  exit 1
fi


function verify_environment() {
  becho "Verifying installation environment"
  if ! ls /sys/firmware/efi/efivars &> /dev/null; then
    recho "Booted in BIOS mode and not UEFI, exiting."
    exit 1
  fi

  if ! ping -W1 -c1 8.8.8.8 &> /dev/null; then
    recho "No Internet access, configure this first for the install."
    exit 1
  fi
}


function enable_ntp() {
  becho "Enabling NTP"
  timedatectl set-ntp true &> /dev/null
}


function configure_disks() {
  becho "Wiping disks"
  wipefs -a "$MAIN_DISK" > /dev/null

  becho "Creating partitions"
  parted -s -a optimal "$MAIN_DISK" mklabel gpt > /dev/null

  # EFI partition
  parted -s -a optimal "$MAIN_DISK" mkpart primary 0% 512MiB > /dev/null
  parted -s "$MAIN_DISK" set 1 esp on > /dev/null
  parted -s "$MAIN_DISK" set 1 boot on > /dev/null
  mkfs.fat -F32 "${MAIN_DISK}1" > /dev/null

  # Root partition
  parted -s -a optimal $MAIN_DISK mkpart primary 512MiB 100% > /dev/null

  becho "Configuring full-disk encryption with LUKS"
  cryptsetup -y -v luksFormat "${MAIN_DISK}2"

  becho "Unlocking newly created LUKS device"
  cryptsetup luksOpen "${MAIN_DISK}2" cryptroot
  mkfs.ext4 /dev/mapper/cryptroot > /dev/null

  # Mount partitions
  mount /dev/mapper/cryptroot /mnt > /dev/null
  mkdir /mnt/boot > /dev/null
  mount "${MAIN_DISK}1" /mnt/boot > /dev/null
}


function install_essential() {
  becho "Installing base system"
  reflector --verbose --latest 5 --sort rate --save /etc/pacman.d/mirrorlist
  pacstrap /mnt base linux linux-firmware efibootmgr
}


function configure_fstab() {
  becho "Creating fstab"
  genfstab -U /mnt >> /mnt/etc/fstab
}


function chroot() {
  arch-chroot /mnt "$@"
}

function chroot_user() {
  chroot su "${USERNAME}" 'bash' "$@"
}


function set_clock() {
  becho "Setting clock"
  chroot ln -sf /usr/share/zoneinfo/Europe/Belfast /etc/localtime > /dev/null
  chroot hwclock --systohc > /dev/null
}


function set_locale_and_keyboard() {
  becho "Setting locale and keyboard"
  chroot bash -c "echo \"en_GB.UTF-8 UTF-8\" > /etc/locale.gen"
  chroot bash -c "echo \"LANG=en_GB.UTF-8\" > /etc/locale.conf"

  chroot bash -c "echo \"KEYMAP=uk\" > /etc/vconsole.conf"
  chroot bash -c "echo \"FONT=latarcyrheb-sun32\" >> /etc/vconsole.conf"
}


function set_hostname() {
  becho "Setting hostname"
  chroot bash -c "echo \"${HOSTNAME}\" > /etc/hostname"
  chroot bash -c "echo \"127.0.0.1  localhost\" > /etc/hosts"
  chroot bash -c "echo \"127.0.1.1  ${HOSTNAME}\" >> /etc/hosts"
}


function configure_mkinitcpio() {
  becho "Updating mkinitcpio"

  # Need 'encrypt' hook for FDE unlock at startup
  chroot sed -i "s#^HOOKS=.*#HOOKS=(base udev autodetect keyboard keymap consolefont modconf block encrypt filesystems fsck)#" /etc/mkinitcpio.conf
  chroot mkinitcpio -P > /dev/null
}


function configure_bootloader() {
  becho "Installing bootloader"

  chroot bash <<'EOF'
    IFS=$'\n'
    for line in $(efibootmgr); do
      if [[ $line == *"Arch Linux"* ]]; then
        entry=$(echo $line | grep -P '(?<=Boot)00..' -o)
        echo "Deleted boot entry \"$line\""
        efibootmgr -b $entry -B > /dev/null
      fi
    done
EOF

  CRYPT_UUID=$(chroot cryptsetup luksUUID "${MAIN_DISK}2")
  chroot efibootmgr --disk ${MAIN_DISK} --part 1 --create --label "Arch Linux" \
    --loader /vmlinuz-linux \
    --unicode "cryptdevice=UUID=${CRYPT_UUID}:cryptroot root=/dev/mapper/cryptroot rw initrd=\initramfs-linux.img" \
    --verbose > /dev/null
}


function set_root_password() {
  becho "Setting root password"
  chroot passwd > /dev/null
}


function create_user() {
  becho "Creating the primary user account"
  chroot useradd -G wheel -m "${USERNAME}"
}


function install_software() {
  becho "Installing most frequently-used software"
  chroot pacman --noconfirm -S \
    bash-completion \
    blueman \
    bluez-utils \
    cinnamon \
    cups \
    curl \
    docker \
    docker-compose \
    feh \
    firefox \
    freerdp \
    gimp \
    gnome-terminal \
    htop \
    iperf3 \
    jq \
    keepassxc \
    libreoffice-fresh \
    mediainfo \
    ncdu \
    nemo-fileroller \
    neovim \
    networkmanager \
    jre-openjdk \
    openssh \
    pacutils \
    pv \
    pwgen \
    python \
    python-pip \
    qalculate-gtk \
    rclone \
    redshift \
    restic \
    rsync \
    sudo \
    nvidia \
    telegram-desktop \
    thunderbird \
    tmux \
    tree \
    ttf-dejavu \
    ttf-liberation \
    ttf-opensans \
    ttf-ubuntu-font-family \
    unionfs-fuse \
    viewnior \
    virtualbox \
    virtualbox-guest-iso \
    virtualbox-host-modules-arch \
    vlc \
    wget \
    wireshark-qt \
    youtube-dl \
    xorg-xinit
}


function do_installation() {
  verify_environment
  enable_ntp
  configure_disks
  install_essential
  configure_fstab
  set_clock
  set_locale_and_keyboard
  set_hostname
  configure_mkinitcpio
  configure_bootloader
  set_root_password
  create_user
  install_software
}


do_installation
